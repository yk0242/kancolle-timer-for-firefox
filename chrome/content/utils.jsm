/* -*- mode: js2;-*- */
// vim: set ts=8 sw=4 sts=4 ff=dos :

var EXPORTED_SYMBOLS = ['KanColleTimerUtils', 'KanColleUtils'];

Components.utils.import("resource://gre/modules/AddonManager.jsm");
Components.utils.import("resource://gre/modules/FileUtils.jsm");

const Cc = Components.classes;
const Ci = Components.interfaces;

var KanColleTimerUtils = {
    addon: {
	get: function(id) {
	    let _addon;
	    AddonManager.getAddonByID(id,
				      function(addon) {
					  _addon = addon;
				      });
	    // Piroさん(http://piro.sakura.ne.jp/)が値が設定されるまで待つことをやっていたので真似してしまう.
	    let thread = Cc['@mozilla.org/thread-manager;1'].getService().mainThread;
	    while (_addon === void(0)) {
		thread.processNextEvent(true);
	    }
	    return _addon;
	},
    },
    console: {
	log: function(str) {
	    let aConsoleService = Cc["@mozilla.org/consoleservice;1"].
		     getService(Ci.nsIConsoleService);
	    aConsoleService.logStringMessage(str);
	},
    },
    alert: {
	show: function(imageURL, title, text, clickable, cookie, listener) {
	    try {
		let alertserv = Cc['@mozilla.org/alerts-service;1']
				    .getService(Ci.nsIAlertsService);
		alertserv.showAlertNotification(imageURL, title, text, clickable, cookie, listener);
	    } catch(e) {
		// prevents runtime error on platforms that don't implement nsIAlertsService
		let image = imageURL;
		KanColleTimerUtils.window.open(null,
					       'chrome://global/content/alerts/alert.xul',
					       '_blank',
					       'chrome,titlebar=no,popup=yes',
					       [image, title, text, clickable, cookie, 0, listener]
					      );
	    }
	},
    },
    sound: {
	play: function(uri) {
	    //let api = KanColleUtils.getBoolPref('sound.api');
	    //if (api) {
		let sound = Cc['@mozilla.org/sound;1']
			    .createInstance(Ci.nsISound);
		sound.play(uri);
	    //} else {
	    //	let sound = new Audio(uri.spec);
	    //	sound.play();
	    //}
	},
    },
    file: {
	initWithPath: function(path) {
	    let file = null;
	    if (path) {
		try {
		    let f = Cc['@mozilla.org/file/local;1']
				.createInstance(Ci.nsILocalFile);
		    f.initWithPath(path);
		    file = f;
		} catch(x) {}
	    }
	    return file;
	},
	createDirectory: function(file, perm) {
	    if (file.exists() && file.isDirectory())
		return false;
	    if (!perm)
		perm = FileUtils.PERMS_DIRECTORY;
	    // if it doesn't exist, create
	    file.create(Ci.nsIFile.DIRECTORY_TYPE, perm);
	    return true;
	},
	profileDir: function() {
	    return Cc["@mozilla.org/file/directory_service;1"]
			.getService(Ci.nsIProperties)
			.get('ProfD', Ci.nsIFile);
	},
	openRawWriter: function(file, mode, perm) {
	    let ret = null;

	    if (!mode)
		mode = FileUtils.MODE_WRONLY|FileUtils.MODE_CREATE|FileUtils.MODE_TRUNCATE;
	    if (!perm)
		perm = FileUtils.PERMS_FILE;

	    try {
		let ostream = Cc['@mozilla.org/network/file-output-stream;1']
				.createInstance(Ci.nsIFileOutputStream);
		ostream.init(file, mode, perm, 0);
		ret = ostream;
	    } catch(x) {}
	    return ret;
	},
	openWriter: function(file, mode, perm) {
	    let ret = null;
	    let ostream = KanColleTimerUtils.file.openRawWriter(file, mode, perm);
	    if (!ostream)
		return null;
	    try {
		let cos = Cc['@mozilla.org/intl/converter-output-stream;1']
			    .createInstance(Ci.nsIConverterOutputStream);
		cos.init(ostream, null, 0,
			 Ci.nsIConverterOutputStream.DEFAULT_REPLACEMENT_CHARACTER);
		ret = cos;
	    } catch(x) {}
	    return ret;
	},
	writeString: function(file, mode, perm, str)
	{
	    let ret = -1;
	    let writer = KanColleTimerUtils.file.openWriter(file, mode, perm);
	    if (!writer)
		return -1;
	    try {
		if (Array.isArray(str)) {
		    str.forEach(function(e) {
			writer.writeString(e);
		    });
		} else {
		    writer.writeString(str);
		}
		ret = 0;
	    } catch(x) {}
	    writer.close();
	    return ret;
	},
	writeObject: function(file, mode, perm, obj) {
	    let str = JSON.stringify(obj);
	    return KanColleTimerUtils.file.writeString(file, mode, perm, str);
	},
	openRawReader: function(file) {
	    let istream = Cc['@mozilla.org/network/file-input-stream;1']
			    .createInstance(Ci.nsIFileInputStream);
	    istream.init(file, FileUtils.MODE_RDONLY, 0, 0);
	    return istream;
	},
	openReader: function(file) {
	    let ret = null;
	    let istream = KanColleTimerUtils.file.openRawReader(file);
	    if (!istream)
		return null;
	    try {
		let cis = Cc['@mozilla.org/intl/converter-input-stream;1']
			    .createInstance(Ci.nsIConverterInputStream);
		cis.init(istream, null, 0,
			 Ci.nsIConverterInputStream.DEFAULT_REPLACEMENT_CHARACTER);
		ret = cis;
	    } catch(x) {}
	    return ret;
	},
	readString: function(file)
	{
	    let str = null;
	    let cis = null;
	    try {
		let s = '';
		let buf = {};
		let hasmore;
		cis = KanColleTimerUtils.file.openReader(file);
		if (cis) {
		    do {
			hasmore = cis.readString(1024, buf);
			s += buf.value;
		    } while(hasmore);
		    str = s;
		}
	    } catch(x) {
	    }
	    if (cis)
		cis.close();
	    return str;
	},
	readObject: function(file) {
	    let str = KanColleTimerUtils.file.readString(file);
	    let obj = undefined;
	    if (str !== null) {
		try {
		    obj = JSON.parse(str);
		} catch(x) {}
	    }
	    return obj;
	},
	saveURI: function(file, url, callback) {
	    if (!file || !url)
		return;
	    let wbp = Cc['@mozilla.org/embedding/browser/nsWebBrowserPersist;1']
		      .createInstance(Ci.nsIWebBrowserPersist);

	    if (callback) {
		wbp.progressListener = {
		    onStateChange: function(aWP, aR, aSF, aS) {
			if (!(aSF & Ci.nsIWebProgressListener.STATE_STOP)) {
			    return;
			}
			callback();
		    },
		};
	    }
	    try {
		wbp.saveURI(url, null, null, null, null, file, null);
	    } catch(x) {
		wbp.saveURI(url, null, null, null, null, null, file, null);
	    }
	},
    },
    prefs: {
	getBranch: function(root) {
	    let prefs = Cc["@mozilla.org/preferences-service;1"]
			.getService(Ci.nsIPrefService);
	    return prefs.getBranch(root);
	},
	getBool: function(branch, leaf, defval) {
	    try {
		return branch.getBoolPref(leaf);
	    } catch(x) {}
	    return defval;
	},
	getInt: function(branch, leaf, defval) {
	    try {
		return branch.getIntPref(leaf);
	    } catch(x) {}
	    return defval;
	},
	getUnichar: function(branch, leaf, defval) {
	    try {
		return branch.getComplexValue(leaf, Ci.nsISupportsString).data;
	    } catch(x) {}
	    return defval;
	},
	getFile: function(branch, leaf) {
	    try {
		return branch.getComplexValue(leaf, Ci.nsILocalFile);
	    } catch(x) {}
	    return null;
	},
	setBool: function(branch, leaf, val) {
	    try {
		branch.setBoolPref(leaf, !!val);
	    } catch(x) {}
	},
	setInt: function(branch, leaf, val) {
	    try {
		branch.setIntPref(leaf, (val | 0));
	    } catch(x) {}
	},
	setUnichar: function(branch, leaf, val) {
	    try {
		let str = Cc["@mozilla.org/supports-string;1"]
			    .createInstance(Ci.nsISupportsString);
		str.data = val;
		branch.setComplexValue(leaf, Ci.nsISupportsString, str);
	    } catch(x) {}
	},
	addObserver: function(branch, leaf, observer, holdweak) {
	    try {
		branch.QueryInterface(Components.interfaces.nsIPrefBranch2);
		branch.addObserver(leaf, observer, holdweak);
	    } catch(x) {}
	},
	removeObserver: function(branch, leaf, observer) {
	    try {
		branch.QueryInterface(Components.interfaces.nsIPrefBranch2);
		branch.removebserver(leaf, observer);
	    } catch(x) {}
	},
    },
    window: {
	getWindowMediator: function() {
	    return Cc["@mozilla.org/appshell/window-mediator;1"]
			.getService(Ci.nsIWindowMediator);
	},
	getBrowserEnumerator: function() {
	    let wm = KanColleTimerUtils.window.getWindowMediator();
	    return wm.getEnumerator("navigator:browser");
	},
	selectTab: function(url) {
	    let browserEnumerator = KanColleTimerUtils.window.getBrowserEnumerator();
	    let selected = false;

	    while(browserEnumerator.hasMoreElements()) {
		let browserInstance = browserEnumerator.getNext().gBrowser;
		// browser インスタンスの全てのタブを確認する.
		let numTabs = browserInstance.tabContainer.childNodes.length;
		for (let index = 0; index < numTabs; index++) {
		    let currentBrowser = browserInstance.getBrowserAtIndex(index);
		    if (currentBrowser.currentURI.spec.indexOf(url) != -1) {
			browserInstance.selectedTab = browserInstance.tabContainer.childNodes[index];
			selected = true;
		    }
		}
	    }
	    return selected;
	},
	findTab: function(url) {
	    let browserEnumerator = KanColleTimerUtils.window.getBrowserEnumerator();
	    let found = null;

	    while(!found && browserEnumerator.hasMoreElements()) {
		let browserInstance = browserEnumerator.getNext().gBrowser;
		// browser インスタンスの全てのタブを確認する.
		let numTabs = browserInstance.tabContainer.childNodes.length;
		for (let index = 0; index < numTabs; index++) {
		    let currentBrowser = browserInstance.getBrowserAtIndex(index);
		    if (currentBrowser.currentURI.spec.indexOf(url) != -1) {
			found = browserInstance.tabContainer.childNodes[index];
			break;
		    }
		}
	    }
	    return found;
	},
	openTab: function(url, hasfocus) {
	    let browserEnumerator = KanColleTimerUtils.window.getBrowserEnumerator();
	    let browserInstance;
	    let tab;

	    while(browserEnumerator.hasMoreElements())
		browserInstance = browserEnumerator.getNext().gBrowser;

	    tab = browserInstance.addTab(url);
	    if (hasfocus)
		browserInstance.selectedTab = tab;

	    return tab;
	},
	findWindow: function(windowName, hasfocus) {
	    let wm = KanColleTimerUtils.window.getWindowMediator();
	    let win = wm.getMostRecentWindow(windowName);
	    if (hasfocus)
		win.focus();
	    return win;
	},
	open: function(parent, url, name, features, args) {
	    let win = Cc['@mozilla.org/embedcomp/window-watcher;1']
			.getService(Ci.nsIWindowWatcher)
			.openWindow(parent, url, name, features, null);
	    win.arguments = args;
	    return win;
	},
    },
    sidebar: {
	toggle: function(win, name) {
	    if (!win)
		return;
	    if (win.SidebarUI && win.SidebarUI.toggle)
		win.SidebarUI.toggle(name);
	    else
		win.toggleSidebar(name);
	},
    },
    screenshot: {
	drawCanvas: function(canvas, size, region, masks) {
	    let ctx;
	    let scale = { x: 1.0, y: 1.0, };

	    if (!canvas || !region)
		return null;

	    if (!size)
		size = {};
	    if (!size.x)
		size.x = region.w;
	    if (!size.y)
		size.y = region.h;

	    scale.x = size.x / region.w;
	    scale.y = size.y / region.h;

	    canvas.style.display = 'inline';
	    canvas.width = size.x;
	    canvas.height = size.y;

	    ctx = canvas.getContext('2d');
	    ctx.clearRect(0, 0, canvas.width, canvas.height);

	    ctx.save();
	    ctx.scale(scale.x, scale.y);

	    // x,y,w,h
	    ctx.drawWindow(region.win, region.x, region.y, region.w, region.h, 'rgb(255,255,255)');

	    if (masks) {
		for (let i = 0; i < masks.length; i++) {
		    let mask = masks[i];
		    ctx.fillStyle = KanColleTimerUtils.color.rgb(mask);
		    ctx.fillRect(mask.x, mask.y, mask.w, mask.h);
		}
	    }
	    ctx.restore();

	    return canvas;
	},
	clearCanvas: function(canvas) {
	    canvas.style.display = 'none';
	    canvas.width = 1;
	    canvas.height = 1;
	},
    },
    timer: {
	startRepeatingEvent: function(observer, interval, data) {
	    let timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
	    let _data = data !== undefined ? data : null;
	    let callback = { notify: function(_timer) { observer.observe(_timer, 'timer-callback', _data); } };
	    if (isNaN(interval))
		throw new TypeError('interval is NaN.');
	    timer.initWithCallback(callback, interval, Ci.nsITimer.TYPE_REPEATING_PRECISE_CAN_SKIP);
	    return timer;
	},
	startDelayedEvent: function(observer, delay, data) {
	    let timer = Cc["@mozilla.org/timer;1"].createInstance(Ci.nsITimer);
	    let _data = data !== undefined ? data : null;
	    let callback = { notify: function(_timer) { observer.observe(_timer, 'timer-callback', _data); } };
	    if (isNaN(delay))
		throw new TypeError('delay is NaN.');
	    timer.initWithCallback(callback, delay, Ci.nsITimer.TYPE_ONE_SHOT);
	    return timer;
	},
	startDelayedEventAt: function(observer, time, data) {
	    let delay;
	    if (isNaN(time))
		throw new TypeError('time is NaN.');
	    delay = time - (new Date).getTime();
	    if (delay < 0)
		delay = 0;
	    return KanColleTimerUtils.timer.startDelayedEvent(observer, delay, data);
	},
	stop: function(timer) {
	    if (!timer)
		return;
	    timer.cancel();
	},
    },
    color: {
	rgb: function(rgb) {
	    return 'rgb(' + [rgb.r, rgb.g, rgb.b].join(',') + ')';
	},
	rgba: function(rgba) {
	    return 'rgba(' + [rgba.r, rgba.g, rgba.b, rgba.a].join(',') + ')';
	},
	rgb2yuv: function(rgb) {
	    return { y:  0.299 * rgb.r +0.587 * rgb.g +0.114 * rgb.b,
		     u: -0.169 * rgb.r -0.331 * rgb.g +0.500 * rgb.b,
		     v:  0.500 * rgb.r -0.419 * rgb.g -0.081 * rgb.b,
	    };
	},
	yuv2hsv: function(yuv) {
	    return { h: yuv.u ? Math.atan2(yuv.v, yuv.u) : 0,
		     s: Math.sqrt(Math.power(yuv.u, 2) + Math.power(yuv.v, 2)),
		     v: yuv.y,
	    };
	},
	rgb2hsv: function(rgb) {
	    let yuv = KanColleTimerUtils.color.rgb2yuv(rgb);
	    return KanColleTimerUtils.color.yuv2hsv(yuv);
	},
    },
};

var KanColleUtils = {
    Title: '\u8266\u3053\u308c\u30bf\u30a4\u30de\u30fc', //艦これタイマー
    IconURL: 'http://pics.dmm.com/freegame/app/854854/200.jpg',
    URL: 'www.dmm.com/netgame/social/-/gadgets/=/app_id=854854',
    PrefRoot: 'extensions.kancolletimer.',
    DataPathPref: 'sync.path',
    DataDir: 'kancolletimer.dat',
    MainWindowName: 'KanColleTimerMainWindow',
    SideBarName: 'viewKanColleTimerSidebar',

    findTab: function() {
	return KanColleTimerUtils.window.findTab(this.URL);
    },
    toggleSidebar: function(win) {
	KanColleTimerUtils.sidebar.toggle(win, this.SideBarName);
    },
    findMainWindow: function(hasfocus) {
	return KanColleTimerUtils.window.findWindow(this.MainWindowName, hasfocus);
    },
    getRegion: function() {
	let region = null;
	try {
	    let tab = this.findTab();
	    let win = tab.linkedBrowser._contentWindow.wrappedJSObject;
	    let game_frame = win.window.document.getElementById('game_frame');
	    let rect = game_frame.getBoundingClientRect();
	    let flash = game_frame.contentWindow.document.getElementById('flashWrap');

	    region = {
		win: win,
		w: flash.clientWidth,
		h: flash.clientHeight,
		x: rect.left + win.pageXOffset + flash.offsetLeft,
		y: rect.top + win.pageYOffset + flash.offsetTop,
	    };
	} catch(x) {
	}
	return region;
    },
    getPrefBranch: function() {
	return KanColleTimerUtils.prefs.getBranch(this.PrefRoot);
    },
    getBoolPref: function(name, defval) {
	let branch = this.getPrefBranch();
	return KanColleTimerUtils.prefs.getBool(branch, name, defval);
    },
    getIntPref: function(name, defval) {
	let branch = this.getPrefBranch();
	return KanColleTimerUtils.prefs.getInt(branch, name, defval);
    },
    getUnicharPref: function(name, defval) {
	let branch = this.getPrefBranch();
	return KanColleTimerUtils.prefs.getUnichar(branch, name, defval);
    },
    getFilePref: function(name) {
	let branch = this.getPrefBranch();
	return KanColleTimerUtils.prefs.getFile(branch, name);
    },
    getUnicharPrefFile: function(name, defval) {
	let branch = this.getPrefBranch();
	let path = KanColleTimerUtils.prefs.getUnichar(branch, name, defval);
	return KanColleTimerUtils.file.initWithPath(path);
    },
    getUnicharPrefFileURI: function(name, defval) {
	let branch = this.getPrefBranch();
	let path = KanColleTimerUtils.prefs.getUnichar(branch, name, defval);
	let file = KanColleTimerUtils.file.initWithPath(path);
	if (!file)
	    return null;
	return Cc['@mozilla.org/network/io-service;1']
		.getService(Ci.nsIIOService)
		.newFileURI(file);
    },
    getUnicharPrefURI: function(name, defval) {
	let branch = this.getPrefBranch();
	let url = KanColleTimerUtils.prefs.getUnichar(branch, name, defval);
	if (!url)
	    return null;
	return Cc['@mozilla.org/network/io-service;1']
		.getService(Ci.nsIIOService)
		.newURI(url, null, null);
    },
    setBoolPref: function(name, val) {
	let branch = this.getPrefBranch();
	return KanColleTimerUtils.prefs.setBool(branch, name, val);
    },
    setIntPref: function(name, val) {
	let branch = this.getPrefBranch();
	return KanColleTimerUtils.prefs.setInt(branch, name, val);
    },
    setUnicharPref: function(name, val) {
	let branch = this.getPrefBranch();
	return KanColleTimerUtils.prefs.setUnichar(branch, name, val);
    },
    getDataDir: function(create) {
	let file = KanColleUtils.getUnicharPrefFile(this.DataPathPref);
	if (!file) {
	    file = KanColleTimerUtils.file.profileDir();
	    file.append(this.DataDir);
	}
	if (create)
	    KanColleTimerUtils.file.createDirectory(file, 0);
	return file;
    },
    getDataFile: function(f) {
	let file = this.getDataDir(true);
	file.append(f);
	return file;
    },
    readObject: function(f, defval) {
	let file = KanColleUtils.getDataFile(f);
	let data = KanColleTimerUtils.file.readObject(file);
	if (data === undefined)
	    data = defval;
	return data;
    },
    writeObject: function(f, data) {
	let file = KanColleUtils.getDataFile(f);
	KanColleTimerUtils.file.writeObject(file, 0, 0, data);
    },
};

