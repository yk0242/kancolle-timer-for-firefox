// vim: set ts=8 sw=4 sts=4 ff=dos :

var KanColleTimerConfig = {
    e10sEnabled: function(){
	return false;
    },

    getBranch:function(){
	return KanColleUtils.getPrefBranch();
    },

    getInt:function(path){
	return KanColleUtils.getIntPref(path, 0);
    },
    setInt:function(path, value){
	KanColleUtils.setIntPref(path, value);
    },

    getBool:function(path){
	return KanColleUtils.getBoolPref(path, false);
    },
    setBool:function(path, value){
	KanColleUtils.setBoolPref(path, value);
    },
    getUnichar:function(path){
	return KanColleUtils.getUnicharPref(path, '');
    },
    setUnichar:function(path,str){
	KanColleUtils.setUnicharPref(path, str);
    },

    register:function(){
	this._branch = this.getBranch();
	KanColleTimerUtils.prefs.addObserver(this._branch, "", this, false);
    },
    unregister:function(){
	if(!this._branch) return;
	KanColleTimerUtils.prefs.removeObserver(this._branch, "", this);
    },

    loadFonts:function(){
	let col = this.getUnichar('display.font-color') || "";
	try{
	    $('sbKanColleTimerSidebar').style.color = col;
	} catch (x) {}
	try{
	    $('kancolletimermainwindow').style.color = col;
	} catch (x) {}
	$('log').style.color = col;

	let font = this.getUnichar("display.font");
	try{
	    $('sbKanColleTimerSidebar').style.fontFamily = font;
	} catch (x) {
	    $('kancolletimermainwindow').style.fontFamily = font;
	}
	$('log').style.fontFamily = font;

	font = this.getUnichar('display.font-size') || "";
	try{
	    $('sbKanColleTimerSidebar').style.fontSize = font;
	} catch (x) {
	    $('kancolletimermainwindow').style.fontSize = font;
	}
	$('log').style.fontSize = font;
    },

    loadPrefs: function(){
	this.loadFonts();

	let b = KanColleTimerConfig.getBool('display.short');
	// fleet-time, ndock-time, kdock-time
	let classname = ['gentimer-time','fleet-time','ndock-time','kdock-time'];
	for(let cn in classname){
	    let elems = document.getElementsByClassName(classname[cn]);
	    for(let i=0; i<elems.length; i++){
		elems[i].style.display = b?"none":"block";
	    }
	}

	let wallpaper = KanColleTimerConfig.getUnichar('wallpaper');
	if( wallpaper ){
	    let alpha = KanColleTimerConfig.getInt('wallpaper.alpha') / 100.0;
	    let sheet = document.styleSheets[1];
	    wallpaper = wallpaper.replace(/\\/g,'/');
	    let rule = "background-image: url('file://"+wallpaper+"'); opacity: "+alpha+";";
	    $('wallpaper').setAttribute('style',rule);
	    //sheet.insertRule(rule,1);
	}

	try{
	    let method = KanColleTimerConfig.getInt('sound.api') ? 'nsisound' : 'html';
	    const sounds = ['ndock', 'kdock', 'mission',
			    '1min.ndock', '1min.kdock', '1min.mission',
			    'default', 'damage-warning'];

	    for (let i = 0; i < sounds.length; i++) {
		let soundid = 'sound.' + sounds[i];
		$(soundid).method = method;
		$(soundid).path = KanColleTimerConfig.getUnichar(soundid);
	    }
	} catch (x) {
	    //AddLog(x);
	}

	KanColleTimerHeadQuarterInfo.update.record.call(KanColleTimerHeadQuarterInfo);
    },

    observe:function(aSubject, aTopic, aData){
	if(aTopic != "nsPref:changed") return;
	this.loadPrefs();
    },

    init: function(){
	try{
	    this.register();
	    this.loadPrefs();
	} catch (x) {
	}
    },
    destroy: function(){
	this.unregister();
    }
};

window.addEventListener("load", function(e){ KanColleTimerConfig.init(); }, false);
window.addEventListener("unload", function(e){ KanColleTimerConfig.destroy(); }, false);
