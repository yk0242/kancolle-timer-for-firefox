// vim: set ts=8 sw=4 sts=4 ff=dos :

Components.utils.import("chrome://kancolletimer/content/httpobserve.jsm");
Components.utils.import("chrome://kancolletimer/content/utils.jsm");

var DropShipList = {
    allships: [],

    clearListBox: function( list ){
	while( list.getRowCount() ){
	    list.removeItemAt( 0 );
	}
    },

    selectAll: function(){
	$('dropship-list').selectAll();
    },

    delete: function(){
	let targets = [].slice.call($('dropship-list' ).selectedItems)
			.map(function(e) {
			    return e.getAttribute('id');
			});
	this.allships = this.allships.filter(function(d) {
	    return targets.every(function(t) { return t != d.id; });
	});
	this.createTable();
	this.save();
    },

    getFile: function(){
	return KanColleUtils.getDataFile('getship.dat');
    },

    openSaveFolder: function(){
	let file = KanColleUtils.getDataDir();
	file.reveal();
    },

    save: function(){
	let file = this.getFile();
	let str = '';

	for( let i = 0; i < this.allships.length; i++ ){
	    let ship = this.allships[i];
	    str += [ship.area, ship.enemy, ship.type, ship.name, ship.date, ship.winrank].map(function(e) {
		    e = e.replace(/[\r\n]+/g, ' ');
		    return e.match(/[",]/) ? '"' + e.replace(/"/g, '""') + '"' : e;
		   }).join(',') + '\n';
	}
	KanColleTimerUtils.file.writeString( file, 0, 0, str );
    },
    load: function(){
	let file = this.getFile();
	let str = KanColleTimerUtils.file.readString(file);
	if (!str)
	    return;

	let d = str.split( "\n" );
	for( let i = 0; i < d.length; i++ ){
	    let data = d[i].split( "," ).map(function(s) {
		let ret = s;
		if (s.match(/^"(.*)"/)) {
		    ret = RegExp.$1.replace(/""/g, '"');
		}
		return ret;
	    });
	    if( data.length < 2 ) continue; // EOFの分をスルー
	    let ship = new Object();
	    ship.area = data[0];
	    ship.enemy = data[1];
	    ship.type = data[2];
	    ship.name = data[3];
	    ship.date = parseFloat( data[4] );
	    ship.id = 'dropship' + i;
	    ship.winrank = data[5] || "";
	    this.allships.push( ship );
	}
    },

    createTable: function(){
	let list = $( 'dropship-list' );
	this.clearListBox( list );

	let n = this.allships.length;

	let ships = this.allships.slice(-500);
	$('number-of-ships').setAttribute('label', ships.length + '/'+n+'人を表示しています');

	let no = 1;
	for( let i = 0; i < ships.length; i++ ){
	    let ship = ships[i];

	    let elem = CreateElement( 'listitem' );
	    let style = no != 1 && (no % 10) == 1 ? "border-top: 1px solid gray;" : "";
	    elem.appendChild( CreateListCell( ship.area == "Created" ? "建造" : ship.area ) );
	    elem.appendChild( CreateListCell( ship.enemy ) );
	    elem.appendChild( CreateListCell( ship.winrank ) );
	    elem.appendChild( CreateListCell( ship.type ) );
	    elem.appendChild( CreateListCell( ship.name ) );
	    elem.appendChild( CreateListCell( GetDateString( ship.date * 1000, true ).replace( "-", "/", "g" ) ) );
	    elem.setAttribute( 'style', style );
	    elem.setAttribute( 'id', ship.id );

	    list.appendChild( elem );
	    no++;
	}
    },

    init: function(){
	this.load();
	this.createTable();
    }

};


window.addEventListener( "load", function( e ){
    DropShipList.init();
}, false );
