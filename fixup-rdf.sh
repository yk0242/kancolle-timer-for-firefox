#!/bin/sh

GIT_DESCRIBE=`git describe --dirty 2>/dev/null`

echo "Version description: ${GIT_DESCRIBE:=UNKNOWN}"
cat install-in.rdf | \
	sed -e "s/&GIT_DESCRIPTION;/${GIT_DESCRIBE}/"  \
	>install.rdf

exit 0
